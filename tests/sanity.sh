#!/bin/bash
#
# This is a simple sanity test to satisfy the RHEL8.1 onboard gating
# requirement.

ret=$?
source /etc/profile.d/modules.sh

module load mpi/mpich-x86_64

mpichversion
let ret=$ret+$?

mpirun hostname
let ret=$ret+$?

exit $ret
